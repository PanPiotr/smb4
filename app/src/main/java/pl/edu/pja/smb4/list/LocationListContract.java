package pl.edu.pja.smb4.list;

import pl.edu.pja.smb4.model.Place;

/**
 * Created by pkuszewski on 03.12.2016.
 */

public interface LocationListContract {

    public interface ListView {

        void addItemToList(Place place);

        void displayCurrentItem(Place place);

        void clear();

    }

    public interface MapView {

        void showPlace(Place place);

        void clear();
    }

    public interface Presenter {

        void startListening();

        void stopListening();

        void addPlace(Place place);

        void loadPlaces();

        void deletePlaces();
    }
}
