package pl.edu.pja.smb4.list;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import lombok.Setter;
import pl.edu.pja.smb4.R;
import pl.edu.pja.smb4.model.Place;
import pl.edu.pja.smb4.repository.Repository;
import pl.edu.pja.smb4.repository.RepositoryContract;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by pkuszewski on 03.12.2016.
 */

public class LocationListPresenter implements LocationListContract.Presenter, GoogleApiClient.ConnectionCallbacks, LocationListener {

    private static final String TAG = LocationListPresenter.class.getSimpleName();
    private RepositoryContract repository;

    @Setter
    private LocationListContract.ListView view;

    @Setter
    private LocationListContract.MapView mapView;

    private GoogleApiClient client;
    private Context context;


    public LocationListPresenter(Context context) {
        this.context = context;
        repository = new Repository(context);
        client = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .build();
    }

    @Override
    public void startListening() {
        client.connect();
    }

    @Override
    public void stopListening() {
        client.disconnect();
    }

    @Override
    public void addPlace(Place place) {
        repository.savePlace(place);
        view.addItemToList(place);
        mapView.showPlace(place);
    }

    @Override
    public void loadPlaces() {
        repository.getPlaces()
                .flatMapIterable(new Func1<List<Place>, Iterable<Place>>() {
                    @Override
                    public Iterable<Place> call(List<Place> places) {
                        return places;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Place>() {
                    @Override
                    public void call(Place place) {
                        view.addItemToList(place);
                        mapView.showPlace(place);
                    }
                });

    }

    @Override
    public void deletePlaces() {
        mapView.clear();
        view.clear();
        repository.deletePlaces();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        LocationRequest request = new LocationRequest();
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        request.setInterval(5000);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.e(TAG, "onConnected: No permission!");
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(client, request, (LocationListener) this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.w(TAG, "onConnectionSuspended: ");
    }

    @Override
    public void onLocationChanged(Location location) {
        Place place = new Place();
        place.setLatitude(location.getLatitude());
        place.setLongitude(location.getLongitude());
        place.setName(context.getString(R.string.new_place));
        place.setDescription(context.getString(R.string.new_place_desc));
        Geocoder coder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = coder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            if (addresses != null && addresses.size() > 0) {
                Address addr = addresses.get(0);
                if (addr.getFeatureName() != null) {
                    place.setName(addr.getFeatureName());
                }
                if (addr.getLocality() != null) {
                    place.setDescription(addr.getLocality());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        view.displayCurrentItem(place);
    }
}
