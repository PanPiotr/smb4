package pl.edu.pja.smb4;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import pl.edu.pja.smb4.list.LocationListFragment;
import pl.edu.pja.smb4.list.LocationListPresenter;
import pl.edu.pja.smb4.list.MapFragment;
import pl.edu.pja.smb4.list.TitledFragment;

public class MainActivity extends AppCompatActivity {

    public static final int REQUEST_CODE = 101;
    @BindView(R.id.activity_main)
    ViewPager pager;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.tabs)
    TabLayout mTabs;
    private HashMap<Integer, Fragment> mapping;
    private FragmentPagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(R.string.app_name);
        prepareMapping();
        permission();
    }

    private void prepareMapping() {
        this.mapping = new HashMap<Integer, Fragment>();
        LocationListFragment fragment = new LocationListFragment();
        LocationListPresenter presenter = new LocationListPresenter(this);
        MapFragment mapFragment = new MapFragment();
        fragment.setPresenter(presenter);
        presenter.setView(fragment);
        presenter.setMapView(mapFragment);
        mapFragment.setPresenter(presenter);
        mapping.put(0, fragment);
        mapping.put(1, mapFragment);
    }

    private void permission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            setupViews();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE);
        }

    }

    private void setupViews() {
        adapter = new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return mapping.get(position);
            }

            @Override
            public int getCount() {
                return mapping.keySet().size();
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return ((TitledFragment) mapping.get(position)).getTitle(MainActivity.this);
            }
        };
        pager.setAdapter(adapter);
        mTabs.setupWithViewPager(pager);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            setupViews();
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(R.string.no_permission);
            builder.setMessage(R.string.no_permisson_desc);
            builder.setPositiveButton(R.string.ok, null);
            builder.show();
        }
    }
}
