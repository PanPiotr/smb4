package pl.edu.pja.smb4.repository.sharedPrefs;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import pl.edu.pja.smb4.model.Place;
import pl.edu.pja.smb4.repository.RepositoryContract;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by pkuszewski on 03.12.2016.
 */

public class SharedPrefsStorage implements RepositoryContract {


    public static final String PLACES_SHARED_PREFS = "places_shared_prefs";
    public static final String PLACES_KEY = "places_key";
    private final Context context;
    private SharedPreferences sharedPrefs;
    private Gson gson;

    public SharedPrefsStorage(Context context) {
        this.context = context;
        sharedPrefs = context.getSharedPreferences(PLACES_SHARED_PREFS, Context.MODE_PRIVATE);
        gson = new Gson();
    }

    @Override
    public Observable<List<Place>> getPlaces() {
        Set<String> placesSet = sharedPrefs.getStringSet(PLACES_KEY, new HashSet<String>());
        return Observable.from(placesSet)
                .map(new Func1<String, Place>() {
                    @Override
                    public Place call(String s) {
                        return gson.fromJson(s, Place.class);
                    }
                }).toList();
    }

    @Override
    public void savePlace(Place place) {
        Set<String> placesSet = new HashSet<>(sharedPrefs.getStringSet(PLACES_KEY, new HashSet<String>()));
        placesSet.add(gson.toJson(place));
        sharedPrefs.edit().putStringSet(PLACES_KEY, placesSet).apply();
    }

    @Override
    public void deletePlaces() {
        sharedPrefs.edit().putStringSet(PLACES_KEY, new HashSet<String>()).apply();
    }
}
