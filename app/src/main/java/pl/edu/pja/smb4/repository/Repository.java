package pl.edu.pja.smb4.repository;

import android.content.Context;

import java.util.List;

import pl.edu.pja.smb4.model.Place;
import pl.edu.pja.smb4.repository.sharedPrefs.SharedPrefsStorage;
import rx.Observable;

/**
 * Created by pkuszewski on 03.12.2016.
 */

public class Repository implements RepositoryContract {

    private SharedPrefsStorage storage;

    public Repository(Context context) {
        storage = new SharedPrefsStorage(context);
    }

    @Override
    public Observable<List<Place>> getPlaces() {
        return storage.getPlaces();
    }

    @Override
    public void savePlace(Place place) {
        storage.savePlace(place);
    }

    @Override
    public void deletePlaces() {
        storage.deletePlaces();
    }
}
