package pl.edu.pja.smb4.model;

import java.util.UUID;

import lombok.Data;

/**
 * Created by pkuszewski on 03.12.2016.
 */
@Data
public class Place {

    private String id = UUID.randomUUID().toString();

    private String name = "";

    private String description = "";

    private double latitude = 0.0f;

    private double longitude = 0.0f;

}
