package pl.edu.pja.smb4.list;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import lombok.Setter;
import pl.edu.pja.smb4.R;
import pl.edu.pja.smb4.model.Place;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapFragment extends SupportMapFragment implements LocationListContract.MapView, TitledFragment {


    @Setter
    LocationListContract.Presenter presenter;

    public MapFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void showPlace(final Place place) {
        getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                LatLng position = new LatLng(place.getLatitude(), place.getLongitude());
                googleMap.addMarker(new MarkerOptions().position(position).title(place.getName()));
                googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(position, 17.0f));
            }
        });
    }

    @Override
    public void clear() {
        getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                googleMap.clear();
            }
        });
    }

    @Override
    public String getTitle(Activity mainActivity) {
        return mainActivity.getString(R.string.map);
    }
}
