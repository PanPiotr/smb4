package pl.edu.pja.smb4.list;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import lombok.Getter;
import lombok.Setter;
import pl.edu.pja.smb4.R;
import pl.edu.pja.smb4.model.Place;

/**
 * Created by pkuszewski on 03.12.2016.
 */

public class LocationListAdapter extends RecyclerView.Adapter<LocationListAdapter.ViewHolder> {


    @Setter
    @Getter
    private List<Place> places = new ArrayList<>();


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.place_list_item_impl, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Place place = getPlaces().get(position);
        holder.mPlaceName.setText(place.getName());
        holder.mPlaceDescription.setText(place.getDescription());
        holder.mPlaceLatitude.setText(String.format(Locale.getDefault(),
                holder.mPlaceLatitude.getContext().getString(R.string.latitude_f), place.getLatitude()));
        holder.mPlaceLongitude.setText(String.format(Locale.getDefault(),
                holder.mPlaceLongitude.getContext().getString(R.string.longitude_f), place.getLongitude()));
    }

    @Override
    public int getItemCount() {
        return places.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.place_name)
        TextView mPlaceName;
        @BindView(R.id.place_description)
        TextView mPlaceDescription;
        @BindView(R.id.place_latitude)
        TextView mPlaceLatitude;
        @BindView(R.id.place_longitude)
        TextView mPlaceLongitude;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
