package pl.edu.pja.smb4.repository;

import java.util.List;

import pl.edu.pja.smb4.model.Place;
import rx.Observable;

/**
 * Created by pkuszewski on 03.12.2016.
 */

public interface RepositoryContract {

    Observable<List<Place>> getPlaces();

    void savePlace(Place place);

    void deletePlaces();
}
