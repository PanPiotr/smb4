package pl.edu.pja.smb4.list;

import android.app.Activity;

/**
 * Created by pkuszewski on 03.12.2016.
 */

public interface TitledFragment {
    String getTitle(Activity mainActivity);
}
