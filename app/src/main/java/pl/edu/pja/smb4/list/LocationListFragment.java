package pl.edu.pja.smb4.list;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Locale;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lombok.Setter;
import pl.edu.pja.smb4.R;
import pl.edu.pja.smb4.model.Place;

/**
 * A simple {@link Fragment} subclass.
 */
public class LocationListFragment extends Fragment implements LocationListContract.ListView, TitledFragment {

    @BindView(R.id.place_name)
    TextView mPlaceName;
    @BindView(R.id.place_description)
    TextView mPlaceDescription;
    @BindView(R.id.place_latitude)
    TextView mPlaceLatitude;
    @BindView(R.id.place_longitude)
    TextView mPlaceLongitude;
    @BindView(R.id.places_list)
    RecyclerView mPlacesList;
    @BindView(R.id.refresh)
    SwipeRefreshLayout mRefresh;
    private LocationListAdapter adapter;
    private Place currentPlace;

    @Setter
    private LocationListPresenter presenter;

    public LocationListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.list_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.delete_menu) {
            presenter.deletePlaces();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_location_list, container, false);
        ButterKnife.bind(this, view);
        setupViews();
        presenter.loadPlaces();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.startListening();
    }

    @Override
    public void onPause() {
        presenter.stopListening();
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        presenter.stopListening();
        super.onDestroyView();
    }

    private void setupViews() {
        adapter = new LocationListAdapter();
        mPlacesList.setAdapter(adapter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mPlacesList.setLayoutManager(layoutManager);
        mRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                adapter.getPlaces().clear();
                adapter.notifyDataSetChanged();
                presenter.loadPlaces();
                mRefresh.setRefreshing(false);
            }
        });
    }

    @Override
    public void addItemToList(Place place) {
        adapter.getPlaces().add(place);
        adapter.notifyItemInserted(adapter.getItemCount() - 1);
    }

    @Override
    public void displayCurrentItem(Place place) {
        this.currentPlace = place;
        mPlaceName.setText(place.getName());
        mPlaceDescription.setText(place.getDescription());
        mPlaceLatitude.setText(String.format(Locale.getDefault(), getString(R.string.latitude_f), place.getLatitude()));
        mPlaceLongitude.setText(String.format(Locale.getDefault(), getString(R.string.longitude_f), place.getLongitude()));
    }

    @Override
    public void clear() {
        adapter.getPlaces().clear();
        adapter.notifyDataSetChanged();
    }

    @OnClick(R.id.place_save)
    public void savePlace() {
        currentPlace.setId(UUID.randomUUID().toString());
        presenter.addPlace(currentPlace);
    }

    @Override
    public String getTitle(Activity mainActivity) {
        return mainActivity.getString(R.string.list);
    }
}
